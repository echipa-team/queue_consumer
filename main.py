#!/usr/bin/env python
import os
import pika
import sys
import json
from twilio.rest import Client


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='167.172.186.195'))
    channel = connection.channel()

    channel.queue_declare(queue='pweb')

    def callback(ch, method, properties, body):
        message_json = json.loads(body.decode())

        account_sid = 'AC6c54d47e2ab82b10aeff1fddd1506b66'
        auth_token = '93bda59d06d5f1d9187cb99d08b00512'
        client = Client(account_sid, auth_token)

        for phone_number in message_json['phone_numbers']:
            try:
                message = client.messages.create(
                    from_='+19705917193',
                    body=message_json['message'],
                    to=phone_number
                )
            except:
                print(' [*] Error: phone is not verified - Trial only')

    channel.basic_consume(queue='pweb', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
