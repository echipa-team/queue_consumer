FROM python:3.8.3-slim-buster

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /src

RUN pip install pika
RUN pip install twilio

COPY main.py /src

CMD [ "python3", "main.py" ]